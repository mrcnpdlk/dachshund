# Raspberry Pi monitor

## Requirements

- NodeJS >= 8.x, you can download it from [here](https://nodejs.org/en/).
- Raspberry Pi (I run and test on version 3 with the Raspbian Lite system)

## Instalation

1. Download or clone repository:

   ```bash
   git clone
   ```

2. Go to project directory.

3. Install dependencies:

   ```bash
   npm install
   ```

   or

   ```bash
   yarn install
   ```

4. Build project:

   ```bash
   npm run build
   ```

   or

   ```bash
   yarn build
   ```

## Configuration

Create configuration file in directory `config` or rename `default.yml`.

## Launch

### Start application

```bash
npm start -- path_to_config_file
```

or

```bash
yarn start -- path_to_config_file
```

### Stop application

```bash
npm run stop
```

or

```bash
yarn stop
```

### Restart application

```bash
npm run restart
```

or

```bash
yarn restart
```

## Release notes

More [here](release_notes.md).

## Author

Damian Macedoński - [dmacedonski@gmail.com](mailto:dmacedonski@gmail.com)

## License

MIT license, more [here](LICENSE).
