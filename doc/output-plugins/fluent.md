# Fluent output plugins

Send data to [Fluent](https://www.fluentd.org/).

## How to use

In configuration use name `fluent_output`

```yaml
tasks:
	- name: fluent_example
	  input: random_input
	  output: fluent_output
	  output_options:
		tag: my_tag
```



## Plugins options

| Name                | Type     | Default     | Description                               |
| ------------------- | -------- | ----------- | ----------------------------------------- |
| `tag`               | `string` | `null`      | Tag name.                                 |
| `tagPrefix`         | `string` | `null`      | [optional] Tag prefix name.               |
| `host`              | `string` | `127.0.0.1` | [optional] Fluent host address.           |
| `port`              | `number` | `24224`     | [optional] Fluent port.                   |
| `timeout`           | `number` | `3`         | [optional] Connection timeout in seconds. |
| `reconnectInterval` | `number` | `600`       | [optional] Reconnection time in seconds.  |

