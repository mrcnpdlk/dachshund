# Speedtest input plugins

Return information about network speed. Require `speedtest-cli`, you can install it on Debian or Ubuntu use command:

```bash
sudo apt-get install speedtest-cli
```

## How to use

Set `speedtest_input` as input name.

```yaml
tasks:
	- name: speedtest_example
	  input: speedtest_input
	  output: stdout_output
```

## Plugins return object structure

Plugin return object.

| Name             | Type     | Description                |
| ---------------- | -------- | -------------------------- |
| `operator`       | `string` | Operator name.             |
| `serverName`     | `string` | Speedtest server name.     |
| `serverLocation` | `string` | Speedtest server location. |
| `download`       | `number` | Download speed in Mbit/s.  |
| `upload`         | `number` | Upload speed in Mbit/s.    |
| `ping`           | `number` | Ping                       |