# Random input plugins

Return random number value from range.

## How to use

Use name `random_input` in configuration. If you want, can set min and max value.

```yaml
tasks:
	- name: random_example
	  input: random_input
	  input_options:
	  	minValue: 1
	  	maxValue: 10
	  output: stdout_output
```

## Plugins options

| Option name | Type     | Default | Description                   |
| ----------- | -------- | ------- | ----------------------------- |
| `minValue`  | `number` | `1`     | [optional] Min random number. |
| `maxValue`  | `number` | `100`   | [optional] Max random number. |

## Plugins return object structure

| Name    | Type     | Description          |
| ------- | -------- | -------------------- |
| `value` | `number` | Random number value. |

