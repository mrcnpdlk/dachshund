# Temperature input plugins

Return information about Raspberry Pi temperature. This plugins use `/opt/vc/bin/vcgencmd` commad to get temperature.

## How to use

Set `temperature_input` as input name.

```yaml
tasks:
	- name: temperature_example
	  input: disk_usage_input
	  output: stdout_output
```

## Plugins return object structure

| Name          | Type     | Description       |
| ------------- | -------- | ----------------- |
| `temperature` | `number` | Temperature in ℃. |

