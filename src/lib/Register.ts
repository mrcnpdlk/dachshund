import InvalidValue from "./exceptions/InvalidValue";
import DiskUsageInput from "./inputs/DiskUsageInput";
import IInput from "./inputs/Input";
import PingInput from "./inputs/PingInput";
import RandomValueInput from "./inputs/RandomValueInput";
import RaspiTemperatureInput from "./inputs/RaspiTemperatureInput";
import RaspiVoltageInput from "./inputs/RaspiVoltageInput";
import SensorsInput from "./inputs/Sensors";
import SpeedTestInput from "./inputs/SpeedTestInput";
import TrafficStats from "./inputs/TrafficStats";
import CliOutput from "./outputs/CliOutput";
import InfluxOutput from "./outputs/InfluxOutput";
import IOutput from "./outputs/Output";

export default class Register {
  private readonly inputs: { [name: string]: any } = {
    random_value: RandomValueInput,
    ping: PingInput,
    raspi_temperature: RaspiTemperatureInput,
    raspi_voltage: RaspiVoltageInput,
    disk_usage: DiskUsageInput,
    speed_test: SpeedTestInput,
    traffic_stats: TrafficStats,
    sensors: SensorsInput,
  };
  private readonly outputs: { [name: string]: any } = {
    cli: CliOutput,
    influx: InfluxOutput,
  };

  getInput(name: string, options = {}): IInput {
    if (!this.inputs.hasOwnProperty(name)) {
      throw InvalidValue.value(name, "input not found");
    }
    return new this.inputs[name](options);
  }

  getOutput(name: string, options = {}): IOutput {
    if (!this.outputs.hasOwnProperty(name)) {
      throw InvalidValue.value(name, "output not found");
    }
    return new this.outputs[name](options);
  }
}
