import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export default class OutputConfigModel {
    @JsonProperty("name", String)
    name: string = null;

    @JsonProperty("options", Object, true)
    options: object = {};
}
