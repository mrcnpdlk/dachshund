import { expect } from "chai";
import { FieldType, Precision } from "influx";
import { JsonConvert } from "json2typescript";
import { describe } from "mocha";
import InvalidValue from "../exceptions/InvalidValue";
import InfluxConfigModel, { PrecisionConverter } from "./InfluxConfigModel";

describe("PrecisionConverter", () => {
    describe("#deserialize", () => {
        let converter: PrecisionConverter = null;

        before(() => {
            converter = new PrecisionConverter();
        });

        it("should return InvalidValue exception if data is not string", () => {
            const func = () => {
                return converter.deserialize(3);
            };

            expect(func).to.throw(InvalidValue);
        });

        it("should return Precision.Nanoseconds if data is 'n'", () => {
            expect(converter.deserialize("n")).to.equal(Precision.Nanoseconds);
        });

        it("should return Precision.Microseconds if data is 'u'", () => {
            expect(converter.deserialize("u")).to.equal(Precision.Microseconds);
        });

        it("should return Precision.Milliseconds if data is 'ms'", () => {
            expect(converter.deserialize("ms")).to.equal(Precision.Milliseconds);
        });

        it("should return Precision.Seconds if data is 's'", () => {
            expect(converter.deserialize("s")).to.equal(Precision.Seconds);
        });

        it("should return Precision.Minutes if data is 'm'", () => {
            expect(converter.deserialize("m")).to.equal(Precision.Minutes);
        });

        it("should return Precision.Hours if data is 'h'", () => {
            expect(converter.deserialize("h")).to.equal(Precision.Hours);
        });

        it("should return InvalidValue exception if data is not Precision", () => {
            const func = () => {
                return converter.deserialize("wrong value");
            };

            expect(func).to.throw(InvalidValue);
        });
    });

    describe("#serialize", () => {
        let converter: PrecisionConverter = null;

        before(() => {
            converter = new PrecisionConverter();
        });

        it("should return 'n' if data is Precision.Nanoseconds", () => {
            expect(converter.serialize(Precision.Nanoseconds)).to.equal("n");
        });

        it("should return 'u' if data is Precision.Microseconds", () => {
            expect(converter.serialize(Precision.Microseconds)).to.equal("u");
        });

        it("should return 'ms' if data is Precision.Milliseconds", () => {
            expect(converter.serialize(Precision.Milliseconds)).to.equal("ms");
        });

        it("should return 's' if data is Precision.Seconds", () => {
            expect(converter.serialize(Precision.Seconds)).to.equal("s");
        });

        it("should return 'm' if data is Precision.Minutes", () => {
            expect(converter.serialize(Precision.Minutes)).to.equal("m");
        });

        it("should return 'h' if data is Precision.Hours", () => {
            expect(converter.serialize(Precision.Hours)).to.equal("h");
        });
    });
});

describe("InfluxConfigModel", () => {
    describe("#extend", () => {
        let config: InfluxConfigModel = null;

        before(() => {
            const converter = new JsonConvert();
            const defaultConfig = converter.deserialize({}, InfluxConfigModel);
            config = defaultConfig.extend({
                port: 8086,
                user: "john",
                database: "test",
                schema: {
                    measurement: "foo",
                    precision: "s",
                    fields: [{
                        name: "value",
                        type: "integer",
                    }],
                    tags: ["tag1", "tag2"],
                },
            });
        });

        it("host should to equal '127.0.0.1'", () => {
            expect(config.host).to.equal("127.0.0.1");
        });

        it("port should to equal 8086", () => {
            expect(config.port).to.equal(8086);
        });

        it("user should to equal 'john'", () => {
            expect(config.user).to.equal("john");
        });

        it("password should to equal ''", () => {
            expect(config.password).to.equal("");
        });

        it("precision should to equal Precision.Seconds", () => {
            expect(config.schema.precision).to.equal(Precision.Seconds);
        });

        it("retentionPolicy should to equal 'DEFAULT'", () => {
            expect(config.schema.retentionPolicy).to.equal("DEFAULT");
        });

        it("database should to equal 'test'", () => {
            expect(config.database).to.equal("test");
        });

        it("measurement should to equal 'foo'", () => {
            expect(config.schema.measurement).to.equal("foo");
        });

        it("fields should to equal one element", () => {
            expect(config.schema.fields).to.lengthOf(1);
        });

        it("field should to be name 'value'", () => {
            expect(config.schema.fields[0].name).to.equal("value");
        });

        it("field should to be type FieldType.INTEGER", () => {
            expect(config.schema.fields[0].type).to.equal(FieldType.INTEGER);
        });
    });
});
