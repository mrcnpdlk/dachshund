export default class InvalidValue extends Error {

    static notNull(name?: string): InvalidValue {
        return new InvalidValue("Must not be null", name);
    }

    static value(value: any, message?: string, name?: string) {
        let fullMessage = value.toString();
        if (message) {
            fullMessage = `${message}: ${fullMessage}`;
        }
        return new InvalidValue(fullMessage, name);
    }

    constructor(message?: string, name?: string) {
        let fullMessage = `Invalid value`;
        if (name) {
            fullMessage += ` (${name})`;
        }
        if (message) {
            fullMessage += `: ${message}`;
        }
        super(fullMessage);
    }
}
