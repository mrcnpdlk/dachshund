import { expect } from "chai";
import { before, describe } from "mocha";
import InvalidValue from "../exceptions/InvalidValue";
import { Duration } from "./Duration";

describe("Duration", () => {
    let duration: Duration = null;

    before(() => {
        duration = new Duration({
            milliseconds: 1,
            seconds: 1,
            minutes: 1,
            hours: 1,
            days: 1,
        });
    });

    it("should return InvalidValue exception if duration is less then 0", () => {
        const func = () => {
            return new Duration({ seconds: -1 });
        };
        expect(func).to.throw(InvalidValue);
    });

    it("milliseconds should to equal 90061001", () => {
        expect(duration.inMilliseconds).to.equal(90061001);
    });

    it("seconds should to equal 90061", () => {
        expect(duration.inSeconds).to.equal(90061);
    });

    it("minutes should to equal 1501", () => {
        expect(duration.inMinutes).to.equal(1501);
    });

    it("hours should to equal 25", () => {
        expect(duration.inHours).to.equal(25);
    });

    it("days should to equal 1", () => {
        expect(duration.inDays).to.equal(1);
    });
});

describe("Timer", () => {
    // describe("#constructor()", () => {
    //     let timer: Timer = null;

    //     before(() => {
    //         timer = new Timer(new Duration({ seconds: 5 }));
    //     });

    //     it("isPeriodic should to equal false", () => {
    //         expect(timer.isPeriodic).to.equal(false);
    //     });

    //     it("duration should to equal duration, 5 seconds", () => {
    //         expect(timer.duration).to.be.instanceof(Duration);
    //         expect(timer.duration.inSeconds).to.equal(5);
    //     });
    // });
});
