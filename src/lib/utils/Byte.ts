import InvalidValue from "../exceptions/InvalidValue";

export default class Byte {
    private readonly bytes: number;

    constructor(size: { bytes?: number, kibibytes?: number, mebibytes?: number, gibibytes?: number, tebibytes?: number }) {
        this.bytes = size.bytes || 0;
        this.bytes += (size.kibibytes || 0) * Math.pow(2, 10);
        this.bytes += (size.mebibytes || 0) * Math.pow(2, 20);
        this.bytes += (size.gibibytes || 0) * Math.pow(2, 30);
        this.bytes += (size.tebibytes || 0) * Math.pow(2, 40);

        if (this.bytes < 0) {
            throw InvalidValue.value(this.bytes, "Size can not be less then 0");
        }
    }

    get inBytes(): number {
        return this.bytes;
    }

    get inKibibytes(): number {
        return Math.floor(this.bytes / Math.pow(2, 10));
    }

    get inMebibytes(): number {
        return Math.floor(this.bytes / Math.pow(2, 20));
    }

    get inGibibytes(): number {
        return Math.floor(this.bytes / Math.pow(2, 30));
    }

    get inTebibytes(): number {
        return Math.floor(this.bytes / Math.pow(2, 40));
    }
}
