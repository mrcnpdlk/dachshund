import InvalidValue from "../exceptions/InvalidValue";

export class Duration {
    private readonly milliseconds: number;

    constructor(time: { milliseconds?: number, seconds?: number, minutes?: number, hours?: number, days?: number } = {}) {
        this.milliseconds = time.milliseconds || 0;
        this.milliseconds += (time.seconds || 0) * 1000;
        this.milliseconds += (time.minutes || 0) * 1000 * 60;
        this.milliseconds += (time.hours || 0) * 1000 * Math.pow(60, 2);
        this.milliseconds += (time.days || 0) * 1000 * Math.pow(60, 2) * 24;

        if (this.milliseconds < 0) {
            throw InvalidValue.value(this.milliseconds, "Duration can not be less then 0");
        }
    }

    get inMilliseconds(): number {
        return this.milliseconds;
    }

    get inSeconds(): number {
        return Math.floor(this.milliseconds / 1000);
    }

    get inMinutes(): number {
        return Math.floor(this.milliseconds / 1000 / 60);
    }

    get inHours(): number {
        return Math.floor(this.milliseconds / 1000 / Math.pow(60, 2));
    }

    get inDays(): number {
        return Math.floor(this.milliseconds / 1000 / Math.pow(60, 2) / 24);
    }
}
