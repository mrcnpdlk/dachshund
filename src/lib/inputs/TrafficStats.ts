import { JsonConvert, JsonObject, JsonProperty } from "json2typescript";
import moment = require("moment");
import Device from "../devices/Device";
import RouterTendaV5 from "../devices/RouterTendaV5";
import InvalidValue from "../exceptions/InvalidValue";
import IPAddress, { IPAddressConverter } from "../utils/IPAddress";
import IInput, { InputData } from "./Input";

@JsonObject
export class TrafficStatsOptions {
    @JsonProperty("model", String)
    model: string = null;

    @JsonProperty("host", IPAddressConverter)
    host: IPAddress = null;
}

export default class TrafficStats implements IInput {
    private options: TrafficStatsOptions;
    private device: Device;

    constructor(options = {}) {
        const convert = new JsonConvert();
        this.options = convert.deserialize(options, TrafficStatsOptions);
        switch (this.options.model) {
            case "router_tenda_v5":
                this.device = new RouterTendaV5(this.options.host);
                break;
            default:
                throw InvalidValue.value(this.options.model, "Device model is invalid, please check supported devices list", "model");
        }
    }

    getData(): Promise<InputData[]> {
        return this.device.getTrafficStats()
            .then(trafficStatsList => {
                const inputData: InputData[] = [];
                const timestamp = moment();
                trafficStatsList.forEach(trafficStats => {
                    inputData.push(new InputData(
                        timestamp,
                        {
                            sentBytes: trafficStats.sentBytes.inBytes,
                            receivedBytes: trafficStats.receivedBytes.inBytes,
                        },
                        {
                            ip: trafficStats.ip.toString(),
                            mac: trafficStats.mac ? trafficStats.mac.toString() : "",
                            name: trafficStats.name || "",
                        },
                    ));
                });
                return Promise.resolve(inputData);
            });
    }
}
