import { expect } from "chai";
import { before, describe, it } from "mocha";
import { InputData } from "./Input";
import RandomValueInput from "./RandomValueInput";

describe("EchoInput", () => {
    describe("#getData()", () => {
        let echoInput: RandomValueInput = null;
        let inputData: InputData = null;

        before(async () => {
            echoInput = new RandomValueInput();
            inputData = await echoInput.getData();
        });

        it("InputData values.value should be a number", () => {
            expect(inputData.values.value).to.be.a("number");
        });
    });
});
