import { exec } from "child_process";
import moment = require("moment");
import IInput, { InputData, ITagList } from "./Input";

export default class SensorsInput implements IInput {
  getData(): Promise<InputData[]> {
    return new Promise((resolve, reject) => {
      exec("sensors -u", (err, stdout) => {
        if (err) {
          reject(err);
        } else {
          const adapters = stdout.split("\n\n");
          const timestamp = moment().utc();
          const inputData: InputData[] = [];
          for (const adapter of adapters) {
            if (adapter) {
              const adapterData = adapter.split("\n");
              const tags: ITagList = {
                name: adapterData[0] || "",
                adapter: adapterData[1]
                  ? adapterData[1].replace("Adapter: ", "")
                  : "",
              };
              let coreName = "";
              for (let i = 2; i < adapterData.length; i++) {
                if (adapterData[i].match(/[\w\d\s]+:$/)) {
                  coreName = adapterData[i].replace(":", "");
                } else if (
                  adapterData[i].match(/[\s]+temp[\d]_input:[\s]{1}/)
                ) {
                  inputData.push(
                    new InputData(
                      timestamp,
                      {
                        temp: parseFloat(
                          adapterData[i].replace(
                            /[\s]+temp[\d]_input:[\s]{1}/,
                            "",
                          ),
                        ),
                      },
                      { ...tags, ...{ coreName } },
                    ),
                  );
                  coreName = "";
                }
              }
            }
          }

          resolve(inputData);
        }
      });
    });
  }
}
