import { Moment } from "moment";

export interface ITagList {
    [key: string]: string;
}

export interface IValueList {
    [key: string]: number | string;
}

export class InputData {
    timestamp: number;
    tags: ITagList;
    values: IValueList;

    constructor(timestamp: Moment, values: IValueList, tags: ITagList = {}) {
        this.timestamp = timestamp.utc().valueOf();
        this.values = values;
        this.tags = tags;
    }
}

export default interface IInput {
    getData(): Promise<InputData | InputData[]>;
}
