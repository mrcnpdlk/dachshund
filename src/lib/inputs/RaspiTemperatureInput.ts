import { exec } from "child_process";
import { JsonConvert, JsonObject, JsonProperty } from "json2typescript";
import moment = require("moment");
import IInput, { InputData } from "./Input";

@JsonObject
class RaspiTemperatureInputOptions {
    /**
     * Path to `vcgencmd`
     * Default: `/opt/vc/bin/vcgencmd`
     */
    @JsonProperty("vcgencmdPath", String, true)
    vcgencmdPath = "/opt/vc/bin/vcgencmd";
}

export default class RaspiTemperatureInput implements IInput {
    options: RaspiTemperatureInputOptions;

    constructor(options = {}) {
        const convert = new JsonConvert();
        this.options = convert.deserialize(options, RaspiTemperatureInputOptions);
    }

    getData(): Promise<InputData> {
        return new Promise((resolve, reject) => {
            exec(`${this.options.vcgencmdPath} measure_temp`, (err, stdout) => {
                if (err) {
                    reject(err);
                    return;
                }
                if (stdout.indexOf("temp") === -1) {
                    reject(new Error("Not result"));
                    return;
                }
                let temp = stdout.replace("temp=", "");
                temp = temp.replace("'C", "");
                resolve(new InputData(moment(), { value: parseFloat(temp) }));
            });
        });
    }
}
