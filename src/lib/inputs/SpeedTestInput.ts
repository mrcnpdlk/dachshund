import { exec } from "child_process";
import moment = require("moment");
import IInput, { InputData } from "./Input";

interface ISpeedTestCli {
    download: number;
    timestamp: string;
    ping: number;
    upload: number;
    server: {
        latency: number;
        name: string;
        url: string;
        country: string;
        lon: string;
        cc: string;
        host: string;
        sponsor: string;
        url2: string;
        lat: string;
        id: string;
        d: number;
    };
}

export default class SpeedTestInput implements IInput {
    getData(): Promise<InputData> {
        return new Promise((resolve, reject) => {
            exec("speedtest-cli --json", (err, stdout) => {
                if (err) {
                    reject(err);
                } else {
                    const data: ISpeedTestCli = JSON.parse(stdout);
                    resolve(new InputData(
                        moment(),
                        {
                            download: data.download,
                            upload: data.upload,
                            ping: data.ping,
                        },
                        {
                            operator: data.server.sponsor,
                            serverName: data.server.sponsor,
                            serverLocation: `${data.server.name} (${data.server.country})`,
                        },
                    ));
                }
            });
        });
    }
}
