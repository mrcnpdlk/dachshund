import { setInterval } from "timers";
import { Logger } from "winston";
import IInput from "./inputs/Input";
import IOutput from "./outputs/Output";
import { Duration } from "./utils/Duration";

export class JobIsRunning extends Error {
    constructor() {
        super("Job is currently running, can not be run again");
    }
}

export default class Task {
    private offset: Duration;
    private name: string;
    private input: IInput;
    private output: IOutput;
    private timer: NodeJS.Timer = null;
    private logger: Logger;

    constructor(offset: Duration, name: string, input: IInput, output: IOutput, logger: Logger) {
        this.offset = offset;
        this.name = name;
        this.input = input;
        this.output = output;
        this.logger = logger;
    }

    run(): void {
        if (this.timer !== null) {
            throw new JobIsRunning();
        }
        this.timer = setInterval(() => {
            this.input.getData()
                .then(inputData => {
                    return this.output.sendData(inputData);
                })
                .then(() => {
                    this.logger.info(`Task ${this.name} completed successfully`);
                })
                .catch(err => {
                    this.logger.error(`Task ${this.name} failed: ${err.message || err}`);
                });
        }, this.offset.inMilliseconds);
    }
}
