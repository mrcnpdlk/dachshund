import Axios, { AxiosInstance } from "axios";
import Byte from "../utils/Byte";
import IPAddress from "../utils/IPAddress";
import MACAddress from "../utils/MACAddress";

export class DeviceTrafficStats {
    ip: IPAddress;
    upload: Byte;
    download: Byte;
    sentBytes: Byte;
    receivedBytes: Byte;
    mac: MACAddress = null;
    name: string = null;

    constructor(ip: IPAddress, upload: Byte, download: Byte, sentBytes: Byte, receivedBytes: Byte) {
        this.ip = ip;
        this.upload = upload;
        this.download = download;
        this.sentBytes = sentBytes;
        this.receivedBytes = receivedBytes;
    }
}

export default abstract class Device {
    protected axios: AxiosInstance;

    protected constructor(host: IPAddress) {
        this.axios = Axios.create({
            baseURL: `http://${host.toString()}`,
        });
    }

    abstract getTrafficStats(): Promise<DeviceTrafficStats[]>;
}
